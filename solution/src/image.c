#include <stdbool.h>
#include <stdlib.h>
#include <string.h>

#include "image.h"

const pixel DEFAULT_PIXEL = { 0, 0, 0 };

image image_duplicate(image src);
bool image_is_point_in_bounds(image img, point p);

image image_create(uint32_t width, uint32_t height) {
    image result = {0};
    result.width = width;
    result.height = height;
    result.data = calloc(width * height, sizeof(pixel)); // https://youtu.be/1stQbTuUBIE
    if (result.data == NULL)
        image_destroy(result);
    return result;
}

void image_destroy(image img) {
    if (img.data != NULL)
        free(img.data);
    img.data = NULL;
}

image image_duplicate(image src) {
    image new_image = image_create(src.width, src.height);
    if (src.data != NULL) {
        for (size_t i = 0; i < src.width*src.height; i++)
            new_image.data[i] = src.data[i];
    }
    return new_image;
}

bool image_is_point_in_bounds(image img, point p) {
    if (img.data == NULL)
        return false;

    return !(p.x < 0 || p.x >= img.width || p.y < 0 || p.y >= img.height);
}

pixel image_get_pixel(image img, point p) {
    if (!image_is_point_in_bounds(img, p))
        return DEFAULT_PIXEL;
    return img.data[img.width*p.y + p.x];
}

void image_set_pixel(image img, point p, pixel pixel) {
    if (!image_is_point_in_bounds(img, p))
        return;
    img.data[img.width*p.y + p.x] = pixel;
}

void image_rotate(image* img) {
    image src_img = image_duplicate(*img);

    uint32_t buffer = img->width;
    img->width = img->height;
    img->height = buffer;

    for (uint32_t x = 0; x < img->width; x++) {
        for (uint32_t y = 0; y < img->height; y++) {
            // Трансформация картинки:
            point dst = {x, y}; // Куда будем писать пиксель
            point src = {img->height - y - 1, x}; // Откуда этот пиксель будет браться на исходной картинке

            // Перемещение пикселей:
            pixel src_pixel = image_get_pixel(src_img, src);
            image_set_pixel(*img, dst, src_pixel);
        }
    }

    image_destroy(src_img);
}
