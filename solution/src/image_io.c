#include <errno.h>
#include <stdlib.h>

#include "image.h"
#include "image_io.h"

static const uint16_t BMP_MAGIC_NUMBER = 0x4D42; // Число, позволяющее отличить формат BMP от остальных

// Описание этого заголовка можно найти в https://en.wikipedia.org/wiki/BMP_file_format
struct __attribute__((packed)) bmp_file_header {
    // Signature (14 bytes)
    uint16_t type;
    uint32_t file_size; 
    uint32_t reserved;
    uint32_t data_start_addr;

    // Info header (40 bytes)
    uint32_t header_size;
    uint32_t width;
    uint32_t height;
    uint16_t color_planes_num;
    uint16_t bits_per_pixel;
    uint32_t compression_method;
    uint32_t image_size;
    uint32_t x_pixels_per_meter;
    uint32_t y_pixels_per_meter;
    uint32_t colors_num;
    uint32_t important_colors_num;
};

// Формат пикселя, специфичный для BMP (тут другой порядок цветов)
struct __attribute__((packed)) bmp_specific_pixel {
    uint8_t b, g, r;
};

uint32_t get_with_bytes_padded(image img) {
    uint32_t width_bytes = img.width*sizeof(struct bmp_specific_pixel); // Ширина картинки в байтах
    return width_bytes % 4 == 0 ? width_bytes : width_bytes - width_bytes%4 + 4; // ...с учётом специфичного для BMP выравнивания
}

image_load_status image_header_check(struct bmp_file_header* header) {
    // Проверяем "магическое число", чтобы понять, что перед нами точно BMP
    if (header->type != BMP_MAGIC_NUMBER)
        return IMAGE_LOAD_TYPE_NOT_SUPPORTED;

    // Лаба умеет работать только с 24-битными картинками
    if (header->bits_per_pixel != 24)
        return IMAGE_LOAD_BPP_NOT_SUPPORTED;

    // У картинки не должно быть сжатия
    if (header->compression_method != 0)
        return IMAGE_LOAD_COMPRESSION_NOT_SUPPORTED;
    return IMAGE_LOAD_OK;
}

int image_load_pixels(FILE* file, image img) {
    uint32_t width_bytes = img.width*sizeof(pixel); // Ширина картинки в байтах
    uint32_t width_bytes_padding = (4 - width_bytes%4)%4; // Особенность, специфичная для BMP: ширина должна быть кратна 4 байтам
    for (uint32_t i = 0; i < img.height; i++) { // Вписываем пиксели в result, соблюдая нужный порядок цветов
        if (fread(&img.data[img.width*(img.height - i - 1)], width_bytes, 1, file) != 1)
            return -1;

        for (uint32_t j = 0; j < img.width; j++) {
            point c = { j, img.height - i - 1 };
            pixel p = image_get_pixel(img, c);
            uint8_t swap = p.r;
            p.r = p.b;
            p.b = swap;
            image_set_pixel(img, c, p);
        }

        // Соблюдаем выравнивание ширины
        if (fseek(file, width_bytes_padding, SEEK_CUR) == -1)
            return -1;
    }
    return 0;
}

image_load_status image_load_bmp(image* dst, char* filename) {
    FILE* file = fopen(filename, "rb");
    if (file == NULL) { // Указатель указывает на NULL, значит что-то пошло не так
        if (errno == ENOENT) // errno показывает конкретную ошибку (в данном случае файл не нашёлся)
            return IMAGE_LOAD_FILE_NOT_EXIST;
        return IMAGE_LOAD_READ_FAIL;
    }

    struct bmp_file_header header;
    // Читаем отдельно заголовок BMP, чтобы понять, сколько места занимает "тело" файла
    if (fread(&header, sizeof(header), 1, file) != 1) {
        fclose(file);
        return IMAGE_LOAD_READ_FAIL;
    }

    image_load_status header_check_rc = image_header_check(&header);
    if (header_check_rc != IMAGE_LOAD_OK) {
        fclose(file);
        return header_check_rc;
    }

    uint32_t pixel_data_start = header.data_start_addr;

    // Сообщаем, откуда в файле в следующий раз надо начать чтение
    if (fseek(file, pixel_data_start, SEEK_SET) == -1) {
        fclose(file);
        return IMAGE_LOAD_READ_FAIL; // Что-то пошло не так
    }

    image result = image_create(header.width, header.height);
    if (result.data == NULL) {
        fclose(file);
        return IMAGE_LOAD_MEMORY_FAIL;
    }

    if (image_load_pixels(file, result) != 0) {
        image_destroy(result);
        fclose(file);
        return IMAGE_LOAD_READ_FAIL; // Что-то пошло не так
    }

    fclose(file);

    *dst = result;
    return IMAGE_LOAD_OK;
}

void image_fill_header(struct bmp_file_header* header, image img) {
    header->type = BMP_MAGIC_NUMBER;
    header->file_size = sizeof(struct bmp_file_header) + get_with_bytes_padded(img) * img.height;
    header->reserved = 0;
    header->data_start_addr = sizeof(struct bmp_file_header);
    header->header_size = 40;
    header->width = img.width;
    header->height = img.height;
    header->color_planes_num = 1;
    header->bits_per_pixel = 24;
    header->compression_method = 0;
    header->image_size = 0;
    header->x_pixels_per_meter = 1;
    header->y_pixels_per_meter = 1;
    header->colors_num = 256*256*256;
    header->important_colors_num = 0;
}

image_save_status image_write(FILE* file, image img) {
    uint32_t width_bytes_padded = get_with_bytes_padded(img);
    // Выделяем память под буфер, из которого потом запишем данные в файл
    struct bmp_specific_pixel* pixel_data = calloc(1, width_bytes_padded);
    // Мы не можем записать данные в файл прямо из img, потому что в BMP цвета пикселей в другом порядке, а картинку в памяти редактировать нельзя

    if (pixel_data == NULL) {
        return IMAGE_SAVE_MEMORY_FAIL;
    }

    // Пишем в массив и файл
    for (uint32_t i = 0; i < img.height; i++) {
        for (uint32_t pixel_index = 0; pixel_index < img.width; pixel_index++) {
            point p = {pixel_index, img.height - i - 1 };
            pixel px = image_get_pixel(img, p);
            pixel_data[pixel_index] = (struct bmp_specific_pixel) {px.b, px.g, px.r };
        }

        if (fwrite(pixel_data, width_bytes_padded, 1, file) != 1) {
            free(pixel_data);
            return IMAGE_SAVE_WRITE_FAIL;
        }
    }
    free(pixel_data);
    return 0;
}

image_save_status image_save_bmp(image img, char* filename) {
    if (img.data == NULL)
        return IMAGE_SAVE_WRITE_FAIL;

    FILE* file = fopen(filename, "wb");

    if (file == NULL) {
        if (errno == EACCES)
            return IMAGE_SAVE_NO_ACCESS;
        return IMAGE_SAVE_OPEN_FAIL;
    }

    // Заполняем заголовок
    struct bmp_file_header header;
    image_fill_header(&header, img);

    if (fwrite(&header, sizeof(header), 1, file) != 1) { // Пишем заголовок в файл
        fclose(file);
        return IMAGE_SAVE_WRITE_FAIL;
    }

    image_save_status image_write_rc = image_write(file, img);
    if (image_write_rc != IMAGE_SAVE_OK) {
        fclose(file);
        return image_write_rc;
    }

    fclose(file);

    return IMAGE_SAVE_OK;
}
