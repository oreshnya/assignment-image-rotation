#include <stdio.h>

#include "image.h"
#include "image_io.h"

int main(int arg_num, char** args) {
    /*
     * Аргументов должно быть хотя бы три:
     * [0] = сама команда
     * [1] = исходный файл
     * [2] = новый файл
     */
    if (arg_num < 3) {
        printf("Вот так надо писать: %s <source> <destination>\n", args[0]);
        return 0;
    }

    char* source = args[1];
    char* destination = args[2];

    image image;
    image_load_status load_status = image_load_bmp(&image, source);
    if (load_status == IMAGE_LOAD_OK) {
        image_rotate(&image);

        image_save_status save_status = image_save_bmp(image, destination);

        if (save_status == IMAGE_SAVE_OK)
            puts("Все готово");
        else if (save_status == IMAGE_SAVE_OPEN_FAIL)
            fprintf(stderr, "Не получилось создать картинку ;(\n");
        else if (save_status == IMAGE_SAVE_MEMORY_FAIL)
            fprintf(stderr, "Злая операционная система не выделяет лабе память Т_Т\n");
        else if (save_status == IMAGE_SAVE_NO_ACCESS)
            fprintf(stderr, "У программы недостаточно прав, чтобы записать в нужный файл ('%s')\n", destination);
        else
            fprintf(stderr, "Не получилось записать в файл '%s'\n", destination);

        image_destroy(image);

    } else if (load_status == IMAGE_LOAD_FILE_NOT_EXIST)
        fprintf(stderr, "Файла с названием '%s' не нашлось :(\n", source);
    else if (load_status == IMAGE_LOAD_COMPRESSION_NOT_SUPPORTED)
        fprintf(stderr, "Эта сжатая картинка. Лаба не рассчитана на работу со сжатыми картинками :(\n");
    else if (load_status == IMAGE_LOAD_BPP_NOT_SUPPORTED)
        fprintf(stderr, "У картинки такой bits-per-pixel, на который эта лаба не рассчитана: используется 24-битные картинки\n");
    else if (load_status == IMAGE_LOAD_MEMORY_FAIL)
        fprintf(stderr, "Злая операционная система не выделяет лабе память Т_Т\n");
    else if (load_status == IMAGE_LOAD_TYPE_NOT_SUPPORTED)
        fprintf(stderr, "У картинки какой-то не такой формат\n");
    else
        fprintf(stderr, "Не получилось прочитать картинку\n");

    return 0;
}

