#pragma once

#include <stdbool.h>
#include <stdint.h>

typedef struct pixel {
    uint8_t r, g, b;
} pixel;

typedef struct point {
    uint32_t x, y;
} point;

typedef struct image image;
struct image {
    uint32_t width, height;
    pixel* data;
};

image image_create(uint32_t width, uint32_t height);
void image_set_pixel(image img, point p, pixel pixel);
pixel image_get_pixel(image img, point p);
void image_destroy(image img);
void image_rotate(image* img);
